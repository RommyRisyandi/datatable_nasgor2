<?php

?>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
 <link rel="stylesheet" type="text/css" href="css/jquery-editable.css">
  
<script type="text/javascript" charset="utf8" src="js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/moment.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jqueryui-editable.js"></script>
<table id="dt-riwayat_kepangkatan">
<thead>
<tr> 
	<th>-</th>
	<th>-</th>
	<th>-</th>
	<th>-</th>
	<th>-</th>
	<th>-</th>
	<th>-</th>
</tr>
</thead>
</table>
<script> 
function rkEditable(){
    $('.pangkat_tb').editable({
        mode: 'inline',
        //emptytext: 'kosong',
        source: function(){
            var p = '';
            $.ajax({
                url: 'scripts/riwayat_kepangkatan.php?view=fillcbopangkat',
                type: 'GET',
                global: false,
                async: false,
                dataType: 'json',
                success:function(data){
                    p = JSON.stringify(data);
                },
                Error: function(xhr){
                    alert(xhr.responseText);
                }
            });
            return p;
        }
    });

    $('.edtmt_pangkat').editable({
                mode: 'inline',
                emptytext: 'kosong',
                format : 'DD / MM / YYYY',
                viewformat : 'DD / MM / YYYY',
                template : 'D / MMMM / YYYY',
                combodate : {
                minYear : 1900,
                maxYear : 2016,
                minuteStep : 1
                }
            });
}

jQuery(document).ready(function($){
    displayriwayat_kepangkatan(''); 
    function displayriwayat_kepangkatan(idp){
        $('#dt-riwayat_kepangkatan').DataTable().destroy();

        var datatableriwayat_kepangkatan = $('#dt-riwayat_kepangkatan').DataTable({
            //"ajax"          : "scripts/riwayat_kepangkatan.php?view=infopegbyid",
            //"scrollY"       : "100%",
            //"scrollX"       : "100%",
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url":  "scripts/riwayat_kepangkatan.php?view=infopegbyid",
				"type": "POST"
			},
			
            "deferRender"   : true,
            "LengthChange"  : true,
            "ordering"      : false,
            "lengthMenu"    : [[10, 25, 50,100, -1], [10, 25, 50, 100, "All"]],
            "autoWidth"     : true,
            "displayLength" : 10,
            "paginate"      : true,
            "oLanguage"     : {
                "sSearch": "Pencarian:",
                "sLoadingRecords"   : "<div class='col-sm-12 text-center'><img src='../assets/img/tunggu.gif' width='50' height='50'/> Mohon tunggu sebentar, kami sedang menyiapkan data anda...</div>",
                "sZeroRecords"      : "Tidak ada Data yang dapat ditampilkan...",
                "sLengthMenu"       : "Tampilkan _MENU_ Baris"

            },

                "columns":[
                {"data" : "nourut", "visible" : true, "width" : "25px", "className": "text-center"},
                {"data" : "nama_pangkat", "visible" : true, "width": "200px"},
                {"data" : "tmt_pangkat", "visible" : true, "width" : "100px", "className": "text-center"},
                {"data" : "no_skpangkat", "visible" : true, "width" : "1%", "className": "text-center"},
                {"data" : "tgl_skpangkat", "visible" : true, "width" : "1%", "className": "text-center"},
                {"data" : "pejabat_skpangkat", "visible" : true, "width" : "2%"},
                {"data" : "dokumen", "visible" : true, "width" : "1%", "className": "text-center"}
                ],
            "drawCallback": function ( settings ) {
				rkEditable();
            }
        });
    }
});
</script>